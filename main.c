#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void affiche_sudoku(int  G[], int n){
    printf("\n");
    int N = n*n;
    for(int i=0; i<N;i++){
        for(int j=0; j<N;j++){
            if (G[j*N+i]==0)
                printf("%s", "_ ");
            else if(G[j*N+i]<10){
                printf("%d ",G[j*N+i]);
            }
            else
                printf("%d",G[j*N+i]);
        }
        printf("\n");
    } 
    return;
}

int valid(int * G, int n, int u, int x){
    int N= n*n;
    int i = u/N; int j = u%N;
    for (int k=0;k <N;k++){
        if (k!=i && G[k*N+j]==x)return -1;
        if (k!=j && G[i*N+k]==x)return -1;
    }
    return 1;
}

int solveSudoku(int *G, int n, int u){
    while (u< n*n*n*n && G[u]!=0)u++;
    
    if (u==n*n*n*n)return 1;

    for(int x =1;x < n*n+1; x++){
        int v = valid(G, n, u, x);
        if (v==1){
            G[u] = x;
            if (solveSudoku(G,n,u+1) ==1){
                return 1;
            }
        }
    }
    G[u] = 0;
    return -1;
}
int parseSudoku(const char * filename,  int * n, int **grid){
    FILE * fp;
    fp = fopen(filename, "r");
    * n = fgetc(fp)-48;
    int N = *n;
    printf("%d\n", N);
    * grid = malloc(N*N*N*sizeof(int));
    char a=0;
    int i=0;
    int digits = 0;
    for(int p = N*N; p>0;p/= 10)digits++;
    while(a != EOF){
        a = fgetc(fp);
        int add = 0;
        if(a>47 && a<58){
            while(a>47 && a<58){
                if(add>0)add*=10;
                add += (int)(a)-48;
                a = fgetc(fp);
            }  
            (*grid)[i] = add;
            i++;
        }
    }
    return 1;
}

int main() {

    const char * filename = "sudokus/sudoku-3-moyen-8.txt";
    int n;
    int * grid;
    
    parseSudoku( filename, &n, &grid);
    affiche_sudoku(grid, n);
    
    clock_t start = clock(), diff;
    int result  = solveSudoku(grid, n, 0);
    diff = clock() - start;
    printf("Time taken %d milliseconds\n", diff);

    if(result==1)printf("- Solution found\n");
    else printf("- No solution\n");
    affiche_sudoku(grid, n);
    
    getchar();
    return 0;
}